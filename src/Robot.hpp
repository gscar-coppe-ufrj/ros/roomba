#pragma once

#include "EKF/EKFilter.hpp"

class RobotEKF : public EKFilter<Eigen::Dynamic, Eigen::Dynamic, Eigen::Dynamic, Eigen::Dynamic, Eigen::Dynamic>
{
public:
    explicit RobotEKF(double sampling_period);

    void setNMeasurses(int n_measures);

    void hasOdom(bool flag);

protected:
    void makeA();
    void makeH();
    void makeW();
    void makeV();
    void makeQ();
    void makeR();

    void makeBaseA();
    void makeBaseH();
    void makeBaseW();
    void makeBaseV();
    void makeBaseQ();
    void makeBaseR();

    void makeProcess();
    void makeMeasure();

    double sampling_period, length;

private:
    int  n_measures;
    bool has_odom;
};
