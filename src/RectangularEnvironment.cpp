#include <stdio.h>
#include <cmath>

#include "RectangularEnvironment.hpp"


RectangularEnvironment::RectangularEnvironment(const ParamContainer & params, const char* database_path) :
    FeatureDatabase(params, database_path)
{
}

std::vector<RectangularEnvironment::Pair> RectangularEnvironment::findMatches()
{
	std::vector<Pair> matches;

    if (available.size() == 0)
    {
		return matches;
	}

	unsigned int k1 = 0;
    for (std::vector<bool>::iterator iter1 = available.begin(); iter1 != available.end()-1; iter1++)
    {
		unsigned int k2 = k1+1;
        for (std::vector<bool>::iterator iter2 = iter1+1; iter2 != available.end(); iter2++)
        {
            if ( (*iter1 && *iter2) && ( (k1 + k2) % 2 != 0 ) )
            {
				Pair match;
				match.ind1 = k1;
				match.ind2 = k2;
				matches.push_back(match);
			}
			k2++;
		}
		k1++;
	}

	return matches;
}

std::vector<Pose> RectangularEnvironment::getMeasures()
{
	std::vector<Pose> measures;

	std::vector<Pair> matches = this->findMatches();
    Line * line1, * line1_new, * line2, * line2_new;
    for (auto iter = matches.begin(); iter != matches.end(); iter++)
    {
		int ind1 = iter->ind1;
		int ind2 = iter->ind2;
		line1 = feature_vec[ind1].feature;
		line2 = feature_vec[ind2].feature;
		line1_new = available_lines[ind1];
		line2_new = available_lines[ind2];
        double x = line1_new->distance - line1->distance;
        double y = line2_new->distance - line2->distance;

		/*
		 *	This code here is specific to the case where the first wall in the database is the one
		 *	normal to the y-axis of the robot, and in its negative direction; the second wall is
		 *	normal to the x-axis, in its positive direction; the third normal to yhe y-axis in its
		 *	positive direction; and the fourth is normal to the x-axis, in its negative direction.
		 */
		Pose pose;
        if (ind1 == 0 && ind2 == 1)
        {
			pose.x   = -y;
			pose.y   =  x;
            pose.the = -std::atan2(line1_new->slope, 1);
        }
        else if (ind1 == 1 && ind2 == 2)
        {
			pose.x = -x;
			pose.y = -y;
            pose.the = -std::atan2(line2_new->slope, 1);
        }
        else if (ind1 == 2 && ind2 == 3)
        {
			pose.x =  y;
			pose.y = -x;
            pose.the = -std::atan2(line1_new->slope, 1);
        }
        else if (ind1 == 0 && ind2 == 3)
        {
			pose.x =  y;
			pose.y =  x;
            pose.the = -std::atan2(line1_new->slope, 1);
        }
        else
        {
			printf("\033[1;31mWARNING: (%d, %d) should not be a match!\033[0m\n", ind1, ind2);
		}

        measures.push_back(pose);
    }

	return measures;
}
