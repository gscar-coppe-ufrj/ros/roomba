#pragma once

#include <ros/ros.h>


struct ParamContainer
{
public:
    double sample_rate;
    double length;
    double laser_ang_min;
    double laser_ang_step;
    double ransac_tol_the;
    double ransac_tol_inl;
    double ransac_tol_prox;
    double tf_laser_base_x;
    double tf_laser_base_y;
    double tf_laser_base_the;
    double tf_odom_base_x;
    double tf_odom_base_y;
    double tf_odom_base_the;
    double tf_world_x0;
    double tf_world_y0;
    double tf_world_the0;
    double accept_laser_delay;

    int laser_samples;
    int ransac_iter;
    int ransac_tol_n_inl;

    bool enable_points;
    bool enable_map;
    bool enable_roomba;
    bool enable_long_lines;
    bool accept_laser;

    ros::NodeHandle node;

    ParamContainer(const ros::NodeHandle & node);
};
