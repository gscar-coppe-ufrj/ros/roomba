#pragma once

#include <Eigen/Core>
#include <Eigen/LU>

#include <iostream>
#include <string>
#include <vector>

/**
 * This is an abstract class that should be inherited by any classes that whishes
 * to implement the Extended Kalman Filter.
 *
 * Note: there is still lots of room for performance improvement.
 */
template <int nX, int nU, int nZ, int nW, int nV> class EKFilter
{
private:
    std::string     sep;
    Eigen::IOFormat fmt;
    std::string     print_color_start;
    std::string     print_color_end;

    Eigen::Matrix<double, nX, nX> eye;

protected:
    Eigen::Matrix<double, nX, nX> A, P;
    Eigen::Matrix<double, nZ, nX> H;
    Eigen::Matrix<double, nX, nW> W;
    Eigen::Matrix<double, nZ, nV> V;
    Eigen::Matrix<double, nW, nW> Q;
    Eigen::Matrix<double, nV, nV> R;
    Eigen::Matrix<double, nX, nZ> K;

    Eigen::Matrix<double, nX, 1> x;
    Eigen::Matrix<double, nU, 1> u;
    Eigen::Matrix<double, nZ, 1> z;


    Eigen::Matrix<double, nX, nX> P_minus;

    Eigen::Matrix<double, nX, 1> x_minus;

public:
    static constexpr std::string COLOR_GRAY()   { return "30"; }
    static constexpr std::string COLOR_RED()    { return "31"; }
    static constexpr std::string COLOR_GREEN()  { return "32"; }
    static constexpr std::string COLOR_YELLOW() { return "33"; }
    static constexpr std::string COLOR_BLUE()   { return "34"; }
    static constexpr std::string COLOR_WHITE()  { return "37"; }

private:
    void predict(Eigen::Matrix<double, nU, 1> u)
    {
        this->u = u;

        this->makeA();
        this->makeW();

        this->makeProcess(); // Compute x_minus

        P_minus = A * P * A.transpose() + W * Q * W.transpose();
    }

    void correct(Eigen::Matrix<double, nZ, 1> z)
    {
        this->makeH();
        this->makeV();
        this->makeR();

        Eigen::Matrix<double, nZ, nZ> tmp = H * P_minus * H.transpose() + V * R * V.transpose();
        K = P_minus * H.transpose() * tmp.inverse();

        this->makeMeasure(); // Compute z

        x = x_minus + K * ( z - this->z );

        P = ( eye - K * H ) * P_minus;
    }

protected:
    EKFilter()
    {
        this->sep = "----------------------------------------\n";
        this->fmt = Eigen::IOFormat(4, 0, ", ", "\n", "[", "]");
        this->disableColor();

        if (nX != Eigen::Dynamic)
        {
            this->eye = Eigen::Matrix<double, nX, nX>::Identity(nX, nX);
        }
    }

    virtual void makeA() = 0;
    virtual void makeH() = 0;
    virtual void makeW() = 0;
    virtual void makeV() = 0;
    virtual void makeQ() = 0;
    virtual void makeR() = 0;

    virtual void makeBaseA() = 0;
    virtual void makeBaseH() = 0;
    virtual void makeBaseW() = 0;
    virtual void makeBaseV() = 0;
    virtual void makeBaseQ() = 0;
    virtual void makeBaseR() = 0;

    virtual void makeProcess() = 0;
    virtual void makeMeasure() = 0;

public:
    void resize(int nnX, int nnU, int nnZ, int nnW, int nnV)
    {
        x.resize(nnX, 1);
        u.resize(nnU, 1);
        z.resize(nnZ, 1);
        W.resize(nnX, nnW);
        V.resize(nnZ, nnV);
        A.resize(nnX, nnX);
        H.resize(nnZ, nnX);
        P.resize(nnX, nnX);
        Q.resize(nnW, nnW);
        R.resize(nnV, nnV);
        K.resize(nnX, nnZ);

        x_minus.resize(nnX, 1);
        P_minus.resize(nnX, nnX);

        eye = Eigen::MatrixXd::Identity(nnX, nnX);
    }

    void init(Eigen::Matrix<double, nX, 1> x0, Eigen::Matrix<double, nX, nX> P0)
    {
        x = x0;
        P = P0;

        this->makeBaseA();
        this->makeBaseH();
        this->makeBaseW();
        this->makeBaseV();
        this->makeBaseQ();
        this->makeBaseR();
    }

    void step(Eigen::Matrix<double, nU, 1> u, Eigen::Matrix<double, nZ, 1> z)
    {
        this->predict(u);
        this->correct(z);
    }

    void step(Eigen::Matrix<double, nU, 1> u)
    {
        this->predict(u);
        x = x_minus;
        P = P_minus;
    }

    void forceUpdateX()
    {
        x = x_minus;
    }

    const Eigen::Matrix<double, nX, 1> getStateMean()
    {
        return x;
    }

    const Eigen::Matrix<double, nX, nX> getStateCovariance()
    {
        return P;
    }

    const Eigen::Matrix<double, nZ, 1> getMeasurement()
    {
        return z;
    }

    void printStateMean(std::ostream & stream)
    {
        stream << sep  << print_color_start << x.format( fmt ) << print_color_end << std::endl;
    }

    void printStateMean()
    {
        printStateMean(std::cout);
    }

    void printStateCovariance(std::ostream & stream)
    {
        stream << sep  << print_color_start << P.format( fmt ) << print_color_end << std::endl;
    }

    void printStateCovariance()
    {
        printStateCovariance(std::cout);
    }

    void printMeasurement(std::ostream & stream)
    {
        stream << sep  << print_color_start << z.format( fmt ) << print_color_end << std::endl;
    }

    void printMeasurement()
    {
        printMeasurement(std::cout);
    }

    void enableColor(std::string color_code)
    {
        std::stringstream stream;
        stream << "\033[1;" << color_code << "m";
        print_color_start = stream.str();
        print_color_end   = "\033[0m";
    }

    void disableColor()
    {
        print_color_start = "\0";
        print_color_end   = "\0";
    }

    Eigen::Matrix<double, nX, 1> mergeStates(std::vector<Eigen::Matrix<double, nX, 1>*> xs, std::vector<Eigen::Matrix<double, nX, nX>*> Ps)
    {
        Eigen::Matrix<double, nX, nX> invP = Eigen::Matrix<double, nX, nX>::Zero();
        Eigen::Matrix<double, nX, 1> x     = Eigen::Matrix<double, nX, 1>::Zero();
        Eigen::Matrix<double, nX, 1> num;
        Eigen::Matrix<double, nX, nX> den;
        auto iter_p = Ps.begin();
        for (auto iter_x = xs.begin(); iter_x != xs.end(); iter_x++)
        {
            x     = (*iter_x);
            invP = (*iter_p).inverse();
            num += x*invP;
            den += invP;
            ++iter_p;
        }
        return den.inverse() * num;
    }
};
