#include "ParamContainer.hpp"


ParamContainer::ParamContainer(const ros::NodeHandle & node)
{
    node.param<double>("model/sample_rate",       sample_rate,          10.0);
    node.param<double>("model/length",            length,               0.235/2);

    node.param<double>("laser/ang_min_deg",       laser_ang_min,        -55.5);
    node.param<double>("laser/ang_step_deg",      laser_ang_step,       0.5);
    node.param<int>   ("laser/samples",           laser_samples,        291);

    node.param<int>   ("ransac/iter",             ransac_iter,          500);
    node.param<double>("ransac/tol_the_deg",      ransac_tol_the,       30.0);
    node.param<double>("ransac/tol_prox",         ransac_tol_prox,      0.1);
    node.param<double>("ransac/tol_inliers",      ransac_tol_inl,       0.01);
    node.param<int>   ("ransac/tol_n_inliers",    ransac_tol_n_inl,     30);

    node.param<bool>  ("rviz/points",             enable_points,        true);
    node.param<bool>  ("rviz/map",                enable_map,           true);
    node.param<bool>  ("rviz/roomba",             enable_roomba,        true);
    node.param<bool>  ("rviz/long_lines",         enable_long_lines,    true);

    node.param<double>("tf/laser_base/x",         tf_laser_base_x,      0.225);
    node.param<double>("tf/laser_base/y",         tf_laser_base_y,      0.0);
    node.param<double>("tf/laser_base/the",       tf_laser_base_the,    -10);

    node.param<double>("tf/odom_base/x",          tf_odom_base_x,       0.0);
    node.param<double>("tf/odom_base/y",          tf_odom_base_y,       0.0);
    node.param<double>("tf/odom_base/the",        tf_odom_base_the,     0.0);

    node.param<double>("tf/world/x",              tf_world_x0,          0.0);
    node.param<double>("tf/world/y",              tf_world_y0,          0.0);
    node.param<double>("tf/world/the",            tf_world_the0,        0.0);

    node.param<bool>  ("ekf/accept_laser",        accept_laser,         false);
    node.param<double>("ekf/accept_laser_delay",  accept_laser_delay,   1.0);

    double deg2rad = M_PI/180;
    laser_ang_min     *= deg2rad;
    laser_ang_step    *= deg2rad;
    ransac_tol_the    *= deg2rad;
    tf_laser_base_the *= deg2rad;
    tf_odom_base_the  *= deg2rad;
}
