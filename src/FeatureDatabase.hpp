#pragma once

#include <vector>

#include "Line.hpp"
#include "ParamContainer.hpp"


class FeatureDatabase
{
protected:
    struct FeatureHolder
    {
        unsigned int count;
        Line * feature;

        FeatureHolder();
    };

    std::vector<FeatureHolder> feature_vec;
    std::vector<bool> available;
    std::vector<Line*> available_lines;

protected:
    inline const std::vector<FeatureHolder> & getFeatures()
    {
        return feature_vec;
    }

public:
    std::vector<double> tol_lines;

    FeatureDatabase(const ParamContainer & params, const char* database_path);
    ~FeatureDatabase();

    /*!
     * Clears the information regarding which lines are currently available to be used as
     * new measurements.
     */
    void clearCache();

    /*!
     * Checks if a feature is in the database.
     *
     * \param feature
     * Feature to be checked inside the database.
     *
     * \return -1, if the feature is not in the databse. Else, return the index of the feature.
     *
     */
    int checkFeature(Line * feature);

    virtual bool addFeature(Line * feature);
    virtual bool * addFeatures(const std::vector<Line*> & features);

    inline const std::vector<Line*> & getActiveWalls()
    {
        return available_lines;
    }
};
