#pragma once

#include "FeatureDatabase.hpp"
#include "Pose.hpp"


class RectangularEnvironment : public FeatureDatabase
{
private:
    struct Pair
    {
        public:
            int ind1, ind2;
    };

public:
    RectangularEnvironment(const ParamContainer & params, const char* database_path);

    std::vector<Pair> findMatches();
    std::vector<Pose> getMeasures();
};
