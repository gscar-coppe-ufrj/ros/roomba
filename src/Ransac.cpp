#include <algorithm>
#include <cmath>
#include <stdlib.h>
#include <random>

#include "Ransac.hpp"
#include "Line.hpp"


const unsigned int Ransac::MAX_ITER_MULT = 10;


Ransac::Ransac(double the_min, double step, int size) :
    lines_freed(true)
{
	this->the_min = the_min;
	this->step    = step;
	this->x_array = new std::vector<double>(size);
	this->y_array = new std::vector<double>(size);
	this->size = size;
	
	/*
	 * Notice that the memory space is reserved here. This approach is taken because
	 * we don't really know how many iterations the user will require, but we assume
	 * it will not surpass 10 times the number of laser measures. This value will be
	 * the maximum number iterations allowed.
	 */
	this->info_ransac.reserve(Ransac::MAX_ITER_MULT * size);
}

void Ransac::setMeasure(std::vector<float> laser_vector)
{
	double theta;
	double depth;
    for (int i = 0; i < this->size; i++)
    {
		depth = laser_vector[i];
		theta = this->the_min + i * this->step;
		(*x_array)[i] = depth * std::cos(theta);
		(*y_array)[i] = depth * std::sin(theta);
	}
}

void Ransac::findInliers(int n, double tol_dist, int tol_inl)
{
    // Saturate if n > maximum number of iterations
	n = (n > Ransac::MAX_ITER_MULT * this->size) ? Ransac::MAX_ITER_MULT * this->size : n;

    // Init several variables, such as: points coordinates and indices
	double x0, y0, x1, y1, x2, y2;
	double slope, delta;
	double distance;
    int count;
	int i1, i2;

    // Init variables needed to select random indices
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, this->size-1);
    std::vector<Line*>::iterator it_new_line_ptr = this->info_ransac.begin();

    // Repeat for the required numbered of trials
    for (int i = 0; i < n; i++)
    {
        // Generate two random indices (points) and store their coordinates
		i1 = dis(gen);
		i2 = dis(gen);
		x1 = (*x_array)[ i1 ];
		y1 = (*y_array)[ i1 ];
		x2 = (*x_array)[ i2 ];
		y2 = (*y_array)[ i2 ];
		
        // Compute the parameters of y = slope * x + delta from the chosen points
		slope = (y2 - y1)/(x2 - x1);
		delta = y1 - slope*x1;

        // Check how many samples are contained in this line (given some tolerance)
        count = 0;
        for (int j = 0; j < this->size; j++)
        {
			x0 = (*x_array)[j];
			y0 = (*y_array)[j];
			distance = this->getDistance(slope, delta, x0, y0);
            if (distance <= tol_dist)
            {
				count++;
			}
		}

        // If the amount of samples (points) contained in the line is enough, save this line
        // Note that several lines representing the same feature may be found. These are later
        // filtered by Ransac::filterInliers
        if (count > tol_inl)
        {
            *it_new_line_ptr = new Line();
            (*it_new_line_ptr)->inliers = count;
            (*it_new_line_ptr)->x[0] = x1;
            (*it_new_line_ptr)->y[0] = y1;
            (*it_new_line_ptr)->x[1] = x2;
            (*it_new_line_ptr)->y[1] = y2;
            (*it_new_line_ptr)->genFromXY();
            it_new_line_ptr++;
		}
	}

    // Store the number of lines found
    this->n_info = std::distance(this->info_ransac.begin(), it_new_line_ptr);

    // Set flag informing that new lines have been created
    lines_freed = false;
}

void Ransac::filterInliers(const std::vector<double> & tol_the)
{
    // Vector that will contain the independent lines
    std::vector<Line*> lines;
	
    // Return if there is only one line
    if ( this->n_info <= 1 ) return;

    // Sort the lines according to the amount of inliers. This will guarantee
    // that the best (containing more inliers) feature is saved
	std::sort( this->info_ransac.begin(),
			   this->info_ransac.begin() + this->n_info,
               [](Line * a, Line * b) { return a->inliers > b->inliers; });

    // Add the first one
	lines.push_back( info_ransac[0] );
    bool flag_similar;
    auto iter_lines_end = this->info_ransac.begin() + this->n_info;

    // Iterate through the current list of lines that may (most likely will) contain repeated lines
    for (auto iter_lines_this = this->info_ransac.begin() + 1; iter_lines_this < iter_lines_end; iter_lines_this++)
    {
        // Iterate through the list of independent liness
        for (auto iter_lines_filter = lines.begin(); iter_lines_filter < lines.end(); iter_lines_filter++)
        {
            // Chek if the lines are similar
            flag_similar = (*iter_lines_this)->similar(*iter_lines_filter, tol_the);

            // If the current line is similar to any other already stored, break and delete it
            if ( flag_similar )
            {
                delete *iter_lines_this;
                *iter_lines_this = 0;
                break;
            }
		}

        // If no similar line was found, save this as a new one (put it in the independent lines list)
        if (!flag_similar)
        {
			lines.push_back( *iter_lines_this );
		}
	}

    // Store the list of independent lines
	std::copy( lines.begin(), lines.end(), this->info_ransac.begin() );
	this->n_info = lines.size();
}

std::vector<Line*> Ransac::getLines(const std::vector<double> & tol)
{
    // Check if lines were freed before this call
    if (!lines_freed)
        deleteLines();

    this->filterInliers(tol);
    std::vector<Line*> ret( this->n_info );
	std::copy( this->info_ransac.begin(), this->info_ransac.begin() + this->n_info, ret.begin() );

    // The lines have been freed, we do not care about deleting them anymore
    lines_freed = true;

	return ret;
}

Ransac::~Ransac()
{
    if (!lines_freed)
        deleteLines();

	delete x_array;
	delete y_array;
}
