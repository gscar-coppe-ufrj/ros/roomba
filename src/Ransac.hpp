#pragma once

#include <vector>

#include "Line.hpp"


class Ransac
{
private:
    std::vector<Line*>   info_ransac;
    std::vector<double>  *x_array;
    std::vector<double>  *y_array;
    double               the_min, step;
    int                  n_info;
    int                  size;
    bool                 lines_freed;

    inline double getDistance(double a, double b, double x0, double y0)
    {
        return std::abs(a*x0 - y0 + b) / std::sqrt(a*a + 1);
    }

    inline void deleteLines()
    {
        for (auto iter = info_ransac.begin(); iter < info_ransac.end(); ++iter)
        {
            delete *iter;
            *iter = 0;
        }
        info_ransac.clear();
        lines_freed = true;
    }

public:
    static const unsigned int MAX_ITER_MULT;

    Ransac(double the_min, double step, int size);
    ~Ransac();

    void setMeasure(std::vector<float> laser_vector);
    void findInliers(int n, double tol_dist, int tol_inl);
    void filterInliers(const std::vector<double> & tol_the);

    /**
     * @brief Returns lines found by the algorithm
     *
     * @param tol Tolerance that determines if two lines are similar.
     *
     * @return The lines found by the algorithm.
     *
     * @see Line::similar()
     *
     * @warning After receiving the lines the caller is responsible for deleting them
     * when they are no longer needed.
     */
    std::vector<Line*> getLines(const std::vector<double> & tol);

    inline const std::vector<double> * const getXArrayPtr() { return (*this).x_array; }
    inline const std::vector<double> * const getYArrayPtr() { return (*this).y_array; }
};
