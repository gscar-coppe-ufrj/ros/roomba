#include <cmath>
#include <iostream>

#include "Line.hpp"


Line::Line()
{
    x[0] = 0;
    x[1] = 0;
    y[0] = 0;
    y[1] = 0;
}

void Line::computeDistanceRelatedMembers()
{
    double den  = slope*slope + 1;
    xp          = -slope*delta/den;
    yp          = delta/den;
    distance    = std::sqrt(xp*xp + yp*yp);
    theta       = std::atan2(delta, -slope*delta);
}

bool Line::similar(const Feature * const line, const std::vector<double> & tol)
{
    // Cast to line. If it is not a line, don't even bother
    const Line * const line_casted = dynamic_cast<const Line * const>(line);
    if (line_casted == 0) return false;

    // Distance from zero. The numerator absolute value is not used to take into account parallel
    // lines on different quadrants.
    double tmp1 = std::sqrt(1 + this->slope * this->slope);
    double d1 = this->delta / tmp1;
    //
    double tmp2 = std::sqrt(1 + line_casted->slope * line_casted->slope);
    double d2 = line_casted->delta / tmp2;
    //
    if (std::abs(d1 - d2) >= tol[1]) return false;

    // Angle between the lines
    double num = std::abs(1 + this->slope * line_casted->slope);
    double den = tmp1 * tmp2;
    //
    double the = std::acos(num/den);
    if (the > tol[0]) return false;

    // If we got here, then the given Line is similar to this one
    return true;
}

void Line::combine(const Line * const other, int n)
{
    slope = (n * slope + other->slope) / (n + 1);
    delta = (n * delta + other->delta) / (n + 1);
    x[0] = std::min(x[0], other->x[0]);
    x[1] = std::max(x[1], other->x[1]);
    y[0]  = slope*x[0] + delta;
    y[1]  = slope*x[1] + delta;
    this->computeDistanceRelatedMembers();
}

void Line::genFromXY()
{
    if (x[0] > x[1])
    {
        std::swap(x[0], x[1]);
        std::swap(y[0], y[1]);
    }
    slope = (x[0] != x[1]) ? (y[1] - y[0])/(x[1] - x[0]) : 1e+6;
    delta = y[0] - slope*x[0];
    this->computeDistanceRelatedMembers();
}

void Line::copy(const Line & line)
{
    x[0] = line.x[0];
    x[1] = line.x[1];
    y[0] = line.y[0];
    y[1] = line.y[1];

    this->genFromXY();
}
