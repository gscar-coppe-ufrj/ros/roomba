#include <cmath>
#include <stdexcept>

#include "Robot.hpp"

#include <iostream>

RobotEKF::RobotEKF(double sampling_period) :
    EKFilter(), sampling_period(sampling_period), n_measures(0), has_odom(false)
{
}

void RobotEKF::setNMeasurses(int n_measures)
{
    if (n_measures == 0)
    {
		throw std::invalid_argument("Number of measures should be > 0.");
	}

    int nnZ = 3 * n_measures;

	z.resize(nnZ, 1);
	H.resize(nnZ, H.cols());
	V = Eigen::MatrixXd::Zero(nnZ, nnZ);
	R = Eigen::MatrixXd::Zero(nnZ, nnZ);

	this->n_measures = n_measures;
}

void RobotEKF::hasOdom(bool flag)
{
	has_odom = flag;
}

void RobotEKF::makeA()
{
    A(0,2) = - sampling_period * std::sin(x(2)) * u(0);
    A(1,2) =   sampling_period * std::cos(x(2)) * u(0);
}

void RobotEKF::makeH()
{
    for (int i = 0; i < n_measures; i++)
    {
		int delta = 3*i;

		H(delta, 0) = 1.0;
		H(delta, 1) = 0.0;
		H(delta, 2) = 0.0;

		H(delta + 1, 0) = 0.0;
		H(delta + 1, 1) = 1.0;
		H(delta + 1, 2) = 0.0;

		H(delta + 2, 0) = 0.0;
		H(delta + 2, 1) = 0.0;
		H(delta + 2, 2) = 1.0;
	}
}

void RobotEKF::makeW()
{
}

void RobotEKF::makeV()
{
	V = Eigen::MatrixXd::Identity(n_measures*3, n_measures*3);
}

void RobotEKF::makeQ()
{
}

void RobotEKF::makeR()
{
    int delta;
    double var_pos, var_the;
    double var_odom_pos  = 0.01 * 0.01;
    double var_odom_the  = M_PI/36;
    double var_laser_pos = 0.2 * 0.2;
    double var_laser_the = M_PI/2;
    for (int i = 0; i < n_measures; i++)
    {
        delta = 3*i;

        if (delta == 0 && has_odom)
        {
            var_pos = var_odom_pos;
            var_the = var_odom_the;
        }
        else
        {
            var_pos = var_laser_pos;
            var_the = var_laser_the;
        }

        R(delta, delta)		= var_pos;
        R(delta, delta + 1) = 0.0;
        R(delta, delta + 2) = 0.0;

        R(delta + 1, delta)		= 0.0;
        R(delta + 1, delta + 1) = var_pos;
        R(delta + 1, delta + 2) = 0.0;

        R(delta + 2, delta)		= 0.0;
        R(delta + 2, delta + 1) = 0.0;
        R(delta + 2, delta + 2) = var_the;
    }
}

void RobotEKF::makeBaseA()
{
    A(0,0) = 1.0;
    A(1,0) = 0.0;
    A(2,0) = 0.0;

    A(0,1) = 0.0;
    A(1,1) = 1.0;
    A(2,1) = 0.0;

    A(0,2) = 0.0;
    A(1,2) = 0.0;
    A(2,2) = 1.0;
}

void RobotEKF::makeBaseH()
{
}

void RobotEKF::makeBaseW()
{
	W(0,0) = 1.0;
	W(1,0) = 0.0;
	W(2,0) = 0.0;

	W(0,1) = 0.0;
	W(1,1) = 1.0;
	W(2,1) = 0.0;

	W(0,2) = 0.0;
	W(1,2) = 0.0;
	W(2,2) = 1.0;
}

void RobotEKF::makeBaseV()
{
}

void RobotEKF::makeBaseQ()
{
	Q(0,0) = 0.01*0.01;
	Q(1,0) = 0.0;
	Q(2,0) = 0.0;

	Q(0,1) = 0.0;
	Q(1,1) = 0.01*0.01;
	Q(2,1) = 0.0;

	Q(0,2) = 0.0;
	Q(1,2) = 0.0;
	Q(2,2) = 0.01*0.01;
}

void RobotEKF::makeBaseR()
{
}

void RobotEKF::makeProcess()
{
    x_minus(0) = x(0) + sampling_period * std::cos(x(2)) * u(0);
    x_minus(1) = x(1) + sampling_period * std::sin(x(2)) * u(0);
    x_minus(2) = x(2) + sampling_period * u(1);
}

void RobotEKF::makeMeasure()
{
    for (int i = 0; i < n_measures; i++)
    {
		int delta = 3*i;

        z(delta)	 = x_minus(0);
        z(delta + 1) = x_minus(1);
        z(delta + 2) = x_minus(2);
	}
}

