#pragma once

#include <atomic>

#include <nav_msgs/Odometry.h>

#include "Pose.hpp"


class CallbackOdometry
{
public:
    CallbackOdometry();
    void operator() (const nav_msgs::Odometry::ConstPtr&);

    inline Pose getMeasure()
    {
        is_ready = false;
        Pose pose(state[0], state[1], state[2]);
        return pose;
    }

    inline bool ready() { return is_ready; }

private:
    double state[3], state_init[3], the_last;
    bool   started;
    int    the_loops;

    std::atomic<bool> is_ready;
};
