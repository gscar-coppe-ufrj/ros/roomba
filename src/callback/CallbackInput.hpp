#pragma once

#include <atomic>

#include <geometry_msgs/Twist.h>


class CallbackInput
{
public:
    CallbackInput();

    inline void operator() (const geometry_msgs::Twist::ConstPtr & msg)
    {
        vx = msg->linear.x;
        wz = msg->angular.z;
    }

    inline double getVx() { return vx; }

    inline double getWz() { return wz; }

private:
    std::atomic<double> vx, wz;
};
