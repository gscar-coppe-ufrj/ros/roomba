#include "callback/CallbackOdometry.hpp"
#include "algebra.h"


CallbackOdometry::CallbackOdometry() :
    started(false), the_last(0), the_loops(0), is_ready(false)
{
    state[0] = 0.0;
    state[1] = 0.0;
    state[2] = 0.0;
}

void CallbackOdometry::operator() (const nav_msgs::Odometry::ConstPtr& msg)
{
    state[0] = msg->pose.pose.position.x;
    state[1] = msg->pose.pose.position.y;
    state[2] = 2*std::atan2(msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
    if ( !started )
    {
        state_init[0] = -state[0];
        state_init[1] = -state[1];
        transform2d(state_init, state_init+1, 0, 0, -state[2]);
        state_init[2] = -state[2];
        started = true;
    }

    // Make orientation measure continuous in (-inf, inf)
    double the_m = state[2];
    int sgn_the		 = (the_m >= 0) - (the_m < 0);
    int sgn_the_last = (the_last >= 0) - (the_last < 0);
    if ( sgn_the != sgn_the_last )
    {
        if ( sgn_the * (the_m - the_last) > M_PI )
        {
            the_loops += sgn_the_last;
        }
        else if (the_loops != 0)
        {
            the_loops += sgn_the;
        }
    }

    int sgn_n = (the_loops > 0) - (the_loops < 0);
    state[2] = M_PI * the_loops + the_m;
    if (sgn_n != sgn_the)
    {
        state[2] += sgn_n * M_PI;
    }
    the_last = the_m;

    //std::transform(state, state+3, state_init, state, std::minus<double>());
    transform2d(state, state+1, state_init[0], state_init[1], state_init[2]);
    state[2] += state_init[2];

    is_ready = true;
}
