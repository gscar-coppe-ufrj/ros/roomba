#include "callback/CallbackLaser.hpp"


CallbackLaser::CallbackLaser(const ParamContainer * params, Ransac * ransac) :
    ransac(ransac), params(params), is_ready(false), lines_freed(true)
{
    tol_the.resize(2);
    tol_the[0] = params->ransac_tol_the;
    tol_the[1] = params->ransac_tol_prox;
}

CallbackLaser::~CallbackLaser()
{
    if (!lines_freed)
        this->deleteLines();
}

void CallbackLaser::operator()(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    ransac->setMeasure(msg->ranges);
    ransac->findInliers(params->ransac_iter, params->ransac_tol_inl, params->ransac_tol_n_inl);
    //
    mtx.lock();
    if (!lines_freed)
    {
        this->deleteLines();
    }
    line_vec = ransac->getLines(tol_the);
    mtx.unlock();
    //
    is_ready = true;
    lines_freed = false;
}
