#pragma once

#include <atomic>
#include <mutex>

#include <sensor_msgs/LaserScan.h>

#include "ParamContainer.hpp"
#include "Ransac.hpp"


class CallbackLaser
{
public:
    CallbackLaser(const ParamContainer * params, Ransac * ransac);
    ~CallbackLaser();

    void operator() (const sensor_msgs::LaserScan::ConstPtr&);

    /**
     * @brief Returns the lines found in the laser scan.
     *
     * @return Lines found in the scan.
     *
     * @warning After receiving the lines the caller is responsible for deleting them
     * when they are no longer needed.
     */
    inline std::vector<Line*> getLines()
    {
        is_ready = false;
        lines_freed = true;
        //
        mtx.lock();
        std::vector<Line*> ret = line_vec;
        mtx.unlock();

        return ret;
    }

    inline bool ready() { return is_ready; }

private:
    std::atomic<bool>      is_ready, lines_freed;
    std::vector<Line*>     line_vec;
    std::vector<double>    tol_the;
    const ParamContainer * params;
    Ransac *               ransac;
    std::mutex             mtx;

    inline void deleteLines()
    {
        for (auto iter = line_vec.begin(); iter < line_vec.end(); ++iter)
        {
            delete *iter;
            *iter = 0;
        }
        line_vec.clear();
        lines_freed = true;
    }
};
