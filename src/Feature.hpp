#pragma once

#include <vector>


class Feature
{
private:
    bool active;
    int occurances;

public:
    Feature();

    /**
     * @brief Checks if two features are similar.
     *
     * Given a certain tolerance, the algorithm checks if a given Feature is similar to this one.
     *
     * @param feature
     * The other feature.
     *
     * @param tol
     * The tolerance.
     *
     * @return true, the feature is similar. False otherwise.
     *
     */
    virtual bool similar(const Feature * const feature, const std::vector<double> & tol) = 0;
};
