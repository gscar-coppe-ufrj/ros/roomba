#pragma once

#include <vector>

#include "Feature.hpp"


class Line : public Feature
{
private:
    void computeDistanceRelatedMembers();

public:
    double slope;
    double delta;
    int    inliers;
    double x[2];
    double y[2];
    double xp;
    double yp;
    double distance;
    double theta;

    Line();

    /**
     * @brief Checks if two lines are similar.
     *
     * Given a tolerance in the angle between the lines, the algorithm checks if they are similar.
     * If the angle is smaller than this threshold the lines are sufficiently close to each other,
     * they are considered similar.
     *
     * @param line The other line.
     *
     * @param tol {[0 ... pi/2], [0 ... inf]} First value sets the angle tolerance and the second
     * sets the distance tolerance.
     *
     * @return true, if lines are similar. False otherwise.
     *
     */
    bool similar(const Feature * const line, const std::vector<double> & tol);

    /**
     * @brief Combines the given Line with this one.
     *
     * Implements a moving mean algorithm to combine this Line with another one.
     *
     * @param other The other line.
     *
     * @param n Number of occurances of this Line (not the other one)
     */
    void combine(const Line * const other, int n);

    void genFromXY();

    void copy(const Line & line);
};
