#pragma once

#include <vector>

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <Eigen/Eigen>

#include "RectangularEnvironment.hpp"


class WorldMap : public RectangularEnvironment
{
private:
    static const float DEFAULT_COLOR_SCAN[];
    static const float DEFAULT_COLOR_LINE_ACTIVE[];
    static const float DEFAULT_COLOR_LINE_INACTIVE[];
    static const float DEFAULT_COLOR_ROOMBA[];
    static const float DEFAULT_COLOR_ROOMBA_VAR[];

    static const int ID_SCAN          = 0;
    static const int ID_WALLS         = 1;
    static const int ID_ROOMBA_CIRCLE = 2;
    static const int ID_ROOMBA_LINE   = 3;
    static const int ID_ROOMBA_VAR    = 4;

    const ParamContainer & params;

    visualization_msgs::Marker marker_scan;
    visualization_msgs::Marker marker_lines;
    visualization_msgs::MarkerArray marker_roomba;
    visualization_msgs::Marker marker_roomba_var;

    ros::Publisher pub_scan;
    ros::Publisher pub_lines;
    ros::Publisher pub_roomba;

    void initPoints(const int size);
    void initLines();
    void initRoomba();

public:
    typedef const std::vector<double> * const LaserPointsPtr;

    WorldMap(ros::NodeHandle & node, const ParamContainer & params, const char * database_path);

    void drawLaserScan(LaserPointsPtr ptr_x_array, LaserPointsPtr ptr_y_array);
    void drawRobot(const Eigen::Matrix3d & variance);
    void drawWorld();

    void eraseLaserScan();

    void shutdown();

    ~WorldMap();
};
