#include "WorldMap.hpp"

#include <visualization_msgs/MarkerArray.h>


const float WorldMap::DEFAULT_COLOR_SCAN[]          = {    0,     0,     1,    1};
const float WorldMap::DEFAULT_COLOR_LINE_ACTIVE[]   = {    1,     0,     0,    1};
const float WorldMap::DEFAULT_COLOR_LINE_INACTIVE[] = {  0.7,     0,     0,    1};
const float WorldMap::DEFAULT_COLOR_ROOMBA[]        = { 0.12, 0.275, 0.882,  0.4};
const float WorldMap::DEFAULT_COLOR_ROOMBA_VAR[]    = {    1, 0.451, 0.933,    1};


WorldMap::WorldMap(ros::NodeHandle & node, const ParamContainer & params, const char * database_path) :
    RectangularEnvironment(params, database_path), params(params)
{
    this->initPoints(params.laser_samples);
    this->initLines();
    this->initRoomba();

    pub_scan   = node.advertise<visualization_msgs::Marker>( "points" , 10 );
    pub_lines  = node.advertise<visualization_msgs::Marker>( "lines"  , 10 );
    pub_roomba = node.advertise<visualization_msgs::MarkerArray>( "roomba" , 10 );
}

WorldMap::~WorldMap()
{
}

void WorldMap::drawLaserScan(LaserPointsPtr ptr_x_array, LaserPointsPtr ptr_y_array)
{
    //if (!params.enable_points) return;

    for (int i = 0; i < params.laser_samples; ++i)
    {
        marker_scan.points[i].x = (*ptr_x_array)[i];
        marker_scan.points[i].y = (*ptr_y_array)[i];
    }
    pub_scan.publish(marker_scan);
}

void WorldMap::drawRobot(const Eigen::Matrix3d & variance)
{
    if (params.enable_roomba)
    {
        marker_roomba_var.scale.x = 2 * variance(0,0);
        marker_roomba_var.scale.y = 2 * variance(1,1);
        //
        if (marker_roomba_var.scale.x < 0.001)
            marker_roomba_var.scale.x = 0.001;
        if (marker_roomba_var.scale.y < 0.001)
            marker_roomba_var.scale.y = 0.001;
        //
        pub_roomba.publish(marker_roomba);
    }
}

void WorldMap::drawWorld()
{
    if (!params.enable_map) return;

    const std::vector<FeatureHolder> & map = getFeatures();
    int N = map.size();
    marker_lines.points.resize(2*N);
    for (int i = 0; i < 2*N; i+=2)
    {
        marker_lines.points[i].x   = map[i/2].feature->x[0];
        marker_lines.points[i].y   = map[i/2].feature->y[0];
        marker_lines.points[i+1].x = map[i/2].feature->x[1];
        marker_lines.points[i+1].y = map[i/2].feature->y[1];
    }
    pub_lines.publish(marker_lines);
}

void WorldMap::eraseLaserScan()
{
    pub_scan.publish(visualization_msgs::Marker());
}

void WorldMap::shutdown()
{
    pub_scan.shutdown();
    pub_lines.shutdown();
    pub_roomba.shutdown();
}

void WorldMap::initPoints(const int size)
{
    marker_scan.header.frame_id = "laser";
    marker_scan.id              = WorldMap::ID_SCAN;
    marker_scan.type            = visualization_msgs::Marker::POINTS;
    marker_scan.action          = visualization_msgs::Marker::ADD;
    marker_scan.scale.x         = 0.02;
    marker_scan.scale.y         = 0.02;
    marker_scan.scale.z         = 0.02;
    marker_scan.color.a         = WorldMap::DEFAULT_COLOR_SCAN[3];
    marker_scan.color.r         = WorldMap::DEFAULT_COLOR_SCAN[0];
    marker_scan.color.g         = WorldMap::DEFAULT_COLOR_SCAN[1];
    marker_scan.color.b         = WorldMap::DEFAULT_COLOR_SCAN[2];

    marker_scan.points.resize(size);
}

void WorldMap::initLines()
{
    marker_lines.header.frame_id = "world";
    marker_lines.id              = WorldMap::ID_WALLS;
    marker_lines.type            = visualization_msgs::Marker::LINE_LIST;
    marker_lines.action          = visualization_msgs::Marker::ADD;
    marker_lines.scale.x         = 0.02;
    marker_lines.scale.y         = 0.02;
    marker_lines.scale.z         = 0.02;
    marker_lines.color.a         = WorldMap::DEFAULT_COLOR_LINE_ACTIVE[3];
    marker_lines.color.r         = WorldMap::DEFAULT_COLOR_LINE_ACTIVE[0];
    marker_lines.color.g         = WorldMap::DEFAULT_COLOR_LINE_ACTIVE[1];
    marker_lines.color.b         = WorldMap::DEFAULT_COLOR_LINE_ACTIVE[2];
}

void WorldMap::initRoomba()
{
    visualization_msgs::Marker circle, line, circle_var;

    circle.header.frame_id = "robot";
    circle.id              = WorldMap::ID_ROOMBA_CIRCLE;
    circle.type            = visualization_msgs::Marker::SPHERE;
    circle.action          = visualization_msgs::Marker::ADD;
    circle.scale.x         = params.length;
    circle.scale.y         = params.length;
    circle.scale.z         = 0.02;
    circle.color.a         = WorldMap::DEFAULT_COLOR_ROOMBA[3];
    circle.color.r         = WorldMap::DEFAULT_COLOR_ROOMBA[0];
    circle.color.g         = WorldMap::DEFAULT_COLOR_ROOMBA[1];
    circle.color.b         = WorldMap::DEFAULT_COLOR_ROOMBA[2];

    line.header.frame_id = "robot";
    line.id              = WorldMap::ID_ROOMBA_LINE;
    line.type            = visualization_msgs::Marker::ARROW;
    line.action          = visualization_msgs::Marker::ADD;
    line.scale.x         = params.length / 2;
    line.scale.y         = 0.05;
    line.scale.z         = 0.02;
    line.color.a         = 1.0;
    line.color.r         = WorldMap::DEFAULT_COLOR_ROOMBA[0];
    line.color.g         = WorldMap::DEFAULT_COLOR_ROOMBA[1];
    line.color.b         = WorldMap::DEFAULT_COLOR_ROOMBA[2];

    marker_roomba_var.header.frame_id = "robot";
    marker_roomba_var.id              = WorldMap::ID_ROOMBA_VAR;
    marker_roomba_var.type            = visualization_msgs::Marker::SPHERE;
    marker_roomba_var.action          = visualization_msgs::Marker::ADD;
    marker_roomba_var.scale.x         = 0.02;
    marker_roomba_var.scale.y         = 0.02;
    marker_roomba_var.scale.z         = 0.02;
    marker_roomba_var.color.a         = WorldMap::DEFAULT_COLOR_ROOMBA_VAR[3];
    marker_roomba_var.color.r         = WorldMap::DEFAULT_COLOR_ROOMBA_VAR[0];
    marker_roomba_var.color.g         = WorldMap::DEFAULT_COLOR_ROOMBA_VAR[1];
    marker_roomba_var.color.b         = WorldMap::DEFAULT_COLOR_ROOMBA_VAR[2];

    marker_roomba.markers.push_back(circle);
    marker_roomba.markers.push_back(line);
    marker_roomba.markers.push_back(marker_roomba_var);
}
