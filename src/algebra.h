template <class Class>
inline void transform2d(Class * x, Class * y, double dx, double dy, double theta)
{
    double cos = std::cos(theta), sin = std::sin(theta);
    double xv = *x, yv=*y;
    *x = cos*xv - sin*yv + dx;
    *y = sin*xv + cos*yv + dy;
}
