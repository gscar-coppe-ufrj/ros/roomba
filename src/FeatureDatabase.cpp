#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "FeatureDatabase.hpp"
#include "rapidxml.hpp"

FeatureDatabase::FeatureHolder::FeatureHolder() :
    count(1)
{
}

FeatureDatabase::FeatureDatabase(const ParamContainer & params, const char* database_path) :
    tol_lines(params.ransac_tol_the)
{
    // Set wall similarity tolerance
    tol_lines.resize(2);
    tol_lines[0] = params.ransac_tol_the;
    tol_lines[1] = params.ransac_tol_prox;

    // Open file and get content
    std::ifstream file;
    file.open(database_path);
    if ( !file.is_open() )
    {
        std::cerr << "\033[1;31mUnable to open XML file\033[0m\n";
        return;
    }
    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();
    std::string content(buffer.str());

    // Initiate XML parser
    rapidxml::xml_document<> doc;
    doc.parse<0>(&content[0]);

    // Read attributes
    std::cout << std::endl << "Reading XML file " << database_path << std::endl;
    rapidxml::xml_node<>* root = doc.first_node();
    root = root->first_node("walls");
    for (rapidxml::xml_node<>* node = root->first_node("wall"); node; node = node->next_sibling())
    {
        Line * line = new Line();
        for (rapidxml::xml_attribute<>* attr = node->first_attribute(); attr; attr = attr->next_attribute())
        {
            std::string name = attr->name();
            double val = atof( attr->value() );
            if (name == "x1")
            {
                line->x[0] = val;
            }
            else if (name == "x2")
            {
                line->x[1] = val;
            }
            else if (name == "y1")
            {
                line->y[0] = val;
            }
            else if (name == "y2")
            {
                line->y[1] = val;
            }
            else
            {
                std::cout << "Found invalid parameter \"" << name << "\" in node \"" << node->name() << "\"" << std::endl;
            }
        }
        line->genFromXY();
        this->addFeature(line);
    }
    std::cout << std::endl << "Done reading." << std::endl << std::endl;
}

void FeatureDatabase::clearCache()
{
    for (std::vector<bool>::iterator iter = available.begin(); iter != available.end(); iter++)
    {
        *iter = false;
    }
}

int FeatureDatabase::checkFeature(Line * feature)
{
    int index = 0;
    for ( auto it = (*this).feature_vec.begin() ; it < (*this).feature_vec.end() ; it++ )
    {
        if ( it->feature->similar(feature, tol_lines) )
        {
            return index;
        }
        index++;
    }
    return -1;
}

bool * FeatureDatabase::addFeatures(const std::vector<Line*> & features) {
    bool ret[features.size()];
    unsigned int k = 0;
    for (auto it = features.begin() ; it < features.end() ; it++)
    {
        ret[k++] = this->addFeature(*it);
    }

    bool * pointer = ret;
    return pointer;
}

bool FeatureDatabase::addFeature(Line * feature)
{
    int index = this->checkFeature(feature);
    if (index == -1)
    {
        // Configure new holder and add it to the feaetures array
        FeatureHolder holder;
        holder.feature = feature;
        feature_vec.push_back(holder);
        available.push_back(true);
        available_lines.push_back(feature);
        return true;
    }
    else
    {
        feature_vec[index].count++;
        feature_vec[index].feature->combine(feature, feature_vec[index].count);
        available[index] = true;
        available_lines[index] = feature_vec[index].feature;
        return false;
    }
}

FeatureDatabase::~FeatureDatabase()
{
    for (auto iter = feature_vec.begin(); iter < feature_vec.end(); ++iter)
    {
        delete iter->feature;
        iter->feature = 0;
    }
    feature_vec.clear();
    available_lines.clear();
}
