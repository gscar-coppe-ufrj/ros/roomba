#include <Eigen/Core>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <string>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <visualization_msgs/Marker.h>

#include "Robot.hpp"
#include "Ransac.hpp"
#include "callback/callbacks.hpp"
#include "ParamContainer.hpp"
#include "WorldMap.hpp"
#include "algebra.h"


RobotEKF init_filter(const ros::NodeHandle& node)
{
    const unsigned int n = 3; // Number of states

    ParamContainer params(node);

    // Instance of the filter
    RobotEKF filter = RobotEKF(1/params.sample_rate);

    // Vector containing the initial state estimate
    Eigen::VectorXd x(n);

    // Matrix of the initial error covariance
    Eigen::MatrixXd P0(n, n);
    P0 << 0.1*0.1, 0.0, 0.0, 0.0, 0.1*0.1, 0.0, 0.0, 0.0, 0.09*0.09;

    // Initialize the filter and the state measurements
    x(0) = 0.0;
    x(1) = 0.0;
    x(2) = 0.0;

    filter.resize(3, 2, 3, 3, 3);
    filter.init(x, P0);

    return filter;
}

int main(int argc, char **argv)
{
    // Init ros
    ros::init(argc, argv, "roomba_ekf");

    // Init node
    ros::NodeHandle node("~");

    // Retrieve parameters from the server
    ParamContainer params(node);

    // Init transform broadcaster and broadcasts constant transforms
    tf::TransformBroadcaster tf_publisher;
    //
    tf::Transform tf_robot2laser(
                tf::Quaternion(tf::Vector3(0.0, 0.0, 1.0), params.tf_laser_base_the),
                tf::Vector3(params.tf_laser_base_x, params.tf_laser_base_y, 0.0)
                );
    tf::Transform tf_world2robot;

    // EKF initialization
    RobotEKF filter = init_filter(node);
    filter.enableColor(filter.COLOR_BLUE());

    // Ransac initialization
    Ransac ransac(params.laser_ang_min, params.laser_ang_step, params.laser_samples);
    // Store pointers to arrays containing markers position
    const std::vector<double> * const ptr_x_array = ransac.getXArrayPtr();
    const std::vector<double> * const ptr_y_array = ransac.getYArrayPtr();

    // RectangularEnvironment initialization
    const std::string path = "/home/jcmonteiro/Programs/catkin_ws/src/roomba/src/config.xml";
    WorldMap world(node, params, path.c_str());

    // Publishers that will provide information regarding Roomba
    ros::Publisher pub_states  = node.advertise<geometry_msgs::Vector3>    ( "states" , 10 );
    ros::Publisher pub_odom    = node.advertise<geometry_msgs::Vector3>    ( "odom"   , 10 );

    // Subscriber that will read the velocity command and update the EKF
    CallbackInput cb_input;
    // Need to define this lambda function so we can hold the state of the functor between calls
    auto callback_input = [&] (const geometry_msgs::Twist::ConstPtr& msg) -> void { cb_input(msg); };
    ros::Subscriber sub_cmd = node.subscribe<geometry_msgs::Twist>( "/cmd_vel" , 1 , callback_input );

    // Subscriber that reads the laser and Ransac algorithm
    CallbackLaser cb_laser(&params, &ransac);
    // Need to define this lambda function so we can hold the state of the functor between calls
    auto callback_laser = [&] (const sensor_msgs::LaserScan::ConstPtr& msg) -> void { cb_laser(msg); };
    ros::Subscriber sub_scan = node.subscribe<sensor_msgs::LaserScan>( "/scan" , 1 , callback_laser );

    // Subscriber that reads the odometry
    CallbackOdometry cb_odometry;
    // Need to define this lambda function so we can hold the state of the functor between calls
    auto callback_odometry = [&] (const nav_msgs::Odometry::ConstPtr& msg) -> void { cb_odometry(msg); };
    ros::Subscriber sub_odom = node.subscribe<nav_msgs::Odometry>( "/odom" , 1 , callback_odometry );

    // Mainloop related variables
    ros::Rate rate(params.sample_rate);
    Eigen::VectorXd u(2);
    Eigen::VectorXd z(3);
    Eigen::VectorXd x(3);
    geometry_msgs::Vector3 states;
    geometry_msgs::Vector3 odom;
    bool new_laser_data;
    std::vector<Line*> vec_line;
    Line * line;
    std::vector<Pose> measures_laser;
    Pose measure_odom;
    //
    bool vehicle_stopped_long_enough = false;
    bool vehicle_moving = true;
    ros::Time time_vehicle_control_to_zero;
    double elapsed_vehicle_still;

    // Rate startup
    rate.reset();
    while ( ros::ok() )
    {
        ros::spinOnce();

        // Input signal
        u(0) = cb_input.getVx();
        u(1) = cb_input.getWz();

        // Check if the laser data should be used
        if (u(0) + u(1) == 0 && !vehicle_stopped_long_enough)
        {
            if (vehicle_moving)
            {
                vehicle_moving = false;
                time_vehicle_control_to_zero = ros::Time::now();
            }
            elapsed_vehicle_still = (ros::Time::now() - time_vehicle_control_to_zero).toSec();
            vehicle_stopped_long_enough = elapsed_vehicle_still >= params.accept_laser_delay;
        }
        else if (!vehicle_moving)
        {
            vehicle_stopped_long_enough = false;
            vehicle_moving = true;
        }

        // Acquire new laser measurements and transform them to the inertial frame
        new_laser_data = cb_laser.ready();
        if (new_laser_data && vehicle_stopped_long_enough) {
            vec_line = cb_laser.getLines();
            for (auto it_line = vec_line.begin(); it_line < vec_line.end(); it_line++)
            {
                line = *it_line;
                transform2d<double>(line->x,   line->y,   params.tf_laser_base_x, params.tf_laser_base_y, params.tf_laser_base_the);
                transform2d<double>(line->x+1, line->y+1, params.tf_laser_base_x, params.tf_laser_base_y, params.tf_laser_base_the);
                transform2d<double>(line->x,   line->y,   x(0), x(1), x(2));
                transform2d<double>(line->x+1, line->y+1, x(0), x(1), x(2));
                line->genFromXY();
            }
            world.addFeatures(vec_line);
        }

        // Measures acquired from the laser
        if (params.accept_laser)
        {
            measures_laser = world.getMeasures();
        }

        // Merge measures acquired from odometry with those from the laser
        int n_laser = measures_laser.size();
        int i = 0;
        bool has_odom = false;
        if (cb_odometry.ready())
        {
            z.resize(3*(n_laser + 1), 1);

            measure_odom = cb_odometry.getMeasure();
            z(0) = measure_odom.x;
            z(1) = measure_odom.y;
            z(2) = measure_odom.the;

            i++;
            has_odom = true;
        }
        else if (params.accept_laser)
        {
            z.resize(3*n_laser, 1);
        }

        if (params.accept_laser)
        {
            for (auto iter = measures_laser.begin(); iter != measures_laser.end(); iter++)
            {
                int delta = 3 * i;
                z(delta)     = iter->x;
                z(delta + 1) = iter->y;
                z(delta + 2) = iter->the;
                i++;
            }
        }

        // Take another step with the EKF
        if (z.rows() == 0)
        {
            filter.step(u);
        }
        else
        {
            filter.setNMeasurses(z.rows()/3);
            filter.hasOdom(has_odom);
            filter.step(u, z);
        }
        //
        x = filter.getStateMean();

        // Publish results
        states.x = x(0);
        states.y = x(1);
        states.z = x(2);
        //
        pub_states.publish(states);
        //
        if (has_odom)
        {
            odom.x = measure_odom.x;
            odom.y = measure_odom.y;
            odom.z = measure_odom.the;
            pub_odom.publish(odom);
        }

        // Set transforms
        tf_publisher.sendTransform( tf::StampedTransform(tf_robot2laser, ros::Time::now(), "robot", "laser") );
        //
        tf_world2robot.setOrigin( tf::Vector3(x(0), x(1), 0) );
        tf_world2robot.setRotation( tf::Quaternion(tf::Vector3(0, 0, 1), x(2)) );
        tf_publisher.sendTransform( tf::StampedTransform(tf_world2robot, ros::Time::now(), "world", "robot") );

        // Send marker info to be plotted by RViz
        if (new_laser_data)
        {
            world.drawLaserScan(ptr_x_array, ptr_y_array);
        }
        else
        {
            world.eraseLaserScan();
        }
        //
        world.drawWorld();
        world.drawRobot(filter.getStateCovariance());

        // Do not forget to clear the cache
        world.clearCache();

        rate.sleep();
    }

    return 0;
}
